#!/usr/bin/env python3

'''
kicad-footprint-generator is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

kicad-footprint-generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with kicad-footprint-generator. If not, see < http://www.gnu.org/licenses/ >.
'''

import sys
import os
#sys.path.append(os.path.join(sys.path[0],"..","..","kicad_mod")) # load kicad_mod path

# export PYTHONPATH="${PYTHONPATH}<path to kicad-footprint-generator directory>"
sys.path.append(os.path.join(sys.path[0], "..", "..", ".."))  # load parent path of KicadModTree
from math import sqrt
import argparse
import yaml
from helpers import *
from KicadModTree import *

sys.path.append(os.path.join(sys.path[0], "..", "..", "tools"))  # load parent path of tools
from footprint_text_fields import addTextFields

series = 'FX20'
series_long = 'FunctionMAX FX20'
manufacturer = 'Hirose'
orientation = 'V'
number_of_rows = 2
datasheet = 'https://www.hirose.com/product/document?clcode=&productname=&series=FX20&documenttype=Catalog&lang=en&documentid=D49373_en'

#Hirose part number
part_code = "FX20-{n:02}P-0.5SV"

pitch = 0.5
pad_size = [0.3, 1.75]
pad_size_paste = [0.28, 1.35]
tab_size = [3, 3.2]
tab_size_paste = [2.98, 3.18]

pins_per_row_range = [20,40,60,80,100,120,140]

def generate_one_footprint(idx, pins, configuration):

    mpn = part_code.format(n=pins)

    # handle arguments
    orientation_str = configuration['orientation_options'][orientation]
    footprint_name = configuration['fp_name_format_string'].format(man=manufacturer,
        series=series,
        mpn=mpn, num_rows=number_of_rows, pins_per_row=int(pins/2), mounting_pad = "",
        pitch=pitch, orientation=orientation_str)

    kicad_mod = Footprint(footprint_name)
    kicad_mod.setAttribute('smd')
    kicad_mod.setDescription("{:s} {:s}, {:s}, {:d} Pins per row ({:s}), generated with kicad-footprint-generator".format(manufacturer, series_long, mpn, pins, datasheet))
    kicad_mod.setTags(configuration['keyword_fp_string'].format(series=series,
        orientation=orientation_str, man=manufacturer,
        entry=configuration['entry_direction'][orientation]))

    ########################## Dimensions ##############################
    A = 17.4 + (idx * 5)
    B = A - 1
    C = A - 12.9

    body_edge_out={
        'left': round(-B/2 ,2),
        'right': round(B/2 ,2),
        'top': -4.35,
        'bottom': 4.35
        }

    D = A - 7

    body_edge_in={
        'left': round(-D/2 ,2),
        'right': round(D/2 ,2),
        'top': -2.4,
        'bottom': 2.4
        }

    tab={
        'left': round(-(A - 3.94)/2 - tab_size[0], 2),
        'right': round((A - 3.94)/2 + tab_size[0], 2),
        'separation': round(A - 3.94 + tab_size[0], 2)
        }

    ############################# Pads ##################################
    #
    # Add pads
    #
    #Pad only with F.Cu and F.Mask
    CPins=int(pins / 2)
    kicad_mod.append(PadArray(start=[-C/2, -5 + pad_size[1] / 2], initial=2,
        pincount=CPins, increment=2,  x_spacing=pitch, size=pad_size,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Cu", "F.Mask"]))

    kicad_mod.append(PadArray(start=[-C/2, 5 - pad_size[1] / 2], initial=1,
        pincount=CPins, increment=2,  x_spacing=pitch, size=pad_size,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Cu", "F.Mask"]))

    kicad_mod.append(PadArray(start=[-tab['separation']/2, 0], initial='', increment=0,
        pincount=2,  x_spacing=tab['separation'], size=tab_size,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Cu", "F.Mask"]))

    #F.Paste
    kicad_mod.append(PadArray(start=[-C/2, -5 + pad_size_paste[1] / 2], initial='', increment=0,
        pincount=CPins,  x_spacing=pitch, size=pad_size_paste,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Paste"]))

    kicad_mod.append(PadArray(start=[-C/2, 5 - pad_size_paste[1] / 2], initial='', increment=0,
        pincount=CPins,  x_spacing=pitch, size=pad_size_paste,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Paste"]))

    kicad_mod.append(PadArray(start=[-tab['separation']/2, 0], initial='', increment=0,
        pincount=2,  x_spacing=tab['separation'], size=tab_size_paste,
        type=Pad.TYPE_SMT, shape=Pad.SHAPE_RECT, layers=["F.Paste"]))

    ######################## Fabrication Layer ###########################
    main_body_out_poly= [
        {'x': body_edge_out['left'] + 1, 'y': body_edge_out['bottom']},
        {'x': body_edge_out['left'], 'y': body_edge_out['bottom'] - 1},
        {'x': body_edge_out['left'], 'y': body_edge_out['top'] + 1},
        {'x': body_edge_out['left'] + 1, 'y': body_edge_out['top']},
        {'x': body_edge_out['right'], 'y': body_edge_out['top']},
        {'x': body_edge_out['right'], 'y': body_edge_out['bottom']},
        {'x': body_edge_out['left'] + 1, 'y': body_edge_out['bottom']}
    ]
    kicad_mod.append(PolygoneLine(polygone=main_body_out_poly,
        width=configuration['fab_line_width'], layer="F.Fab"))

    main_body_in_poly= [
        {'x': body_edge_in['left'] + 1, 'y': body_edge_in['bottom']},
        {'x': body_edge_in['left'] + 1, 'y': body_edge_in['bottom'] - 1},
        {'x': body_edge_in['left'], 'y': body_edge_in['bottom'] - 1},
        {'x': body_edge_in['left'], 'y': body_edge_in['top'] + 1},
        {'x': body_edge_in['left'] + 1, 'y': body_edge_in['top']},
        {'x': body_edge_in['right'] - 1, 'y': body_edge_in['top']},
        {'x': body_edge_in['right'], 'y': body_edge_in['top'] + 1},
        {'x': body_edge_in['right'], 'y': body_edge_in['bottom'] - 1},
        {'x': body_edge_in['right'] - 1, 'y': body_edge_in['bottom'] - 1},
        {'x': body_edge_in['right'] - 1, 'y': body_edge_in['bottom']},
        {'x': body_edge_in['left'] + 1, 'y': body_edge_in['bottom']}
    ]
    kicad_mod.append(PolygoneLine(polygone=main_body_in_poly,
        width=configuration['fab_line_width'], layer="F.Fab"))

    main_arrow_poly= [
        {'x': (-C/2)-0.2, 'y': body_edge_out['bottom'] + 0.875},
        {'x': -C/2, 'y': 5},
        {'x': (-C/2)+0.2, 'y': body_edge_out['bottom'] + 0.875},
        {'x': (-C/2)-0.2, 'y': body_edge_out['bottom'] + 0.875}
    ]
    kicad_mod.append(PolygoneLine(polygone=main_arrow_poly,
        width=configuration['fab_line_width'], layer="F.Fab"))

    ######################## SilkS Layer ###########################
    offset = (pad_size[0]/2)+0.2+.06

    poly_left_bottom = [
        {'x': -(C/2) - offset, 'y': body_edge_out['bottom'] + (pad_size[1]/3)},
        {'x': -(C/2) - offset, 'y': body_edge_out['bottom'] + configuration['silk_fab_offset']},
        {'x': body_edge_out['left'] + 1 - configuration['silk_fab_offset'], 'y': body_edge_out['bottom'] + configuration['silk_fab_offset']},
        {'x': body_edge_out['left'] - configuration['silk_fab_offset'], 'y': body_edge_out['bottom'] - 1 + configuration['silk_fab_offset']},
        {'x': body_edge_out['left'] - configuration['silk_fab_offset'], 'y': tab_size[1] / 2 + configuration['silk_fab_offset']}
    ]

    kicad_mod.append(PolygoneLine(polygone=poly_left_bottom,
        width=configuration['silk_line_width'], layer="F.SilkS"))

    poly_left_top = [
        {'x': body_edge_out['left'] - configuration['silk_fab_offset'], 'y': -tab_size[1] / 2 - configuration['silk_fab_offset']},
        {'x': body_edge_out['left'] - configuration['silk_fab_offset'], 'y': body_edge_out['top'] + 1 - configuration['silk_fab_offset']},
        {'x': body_edge_out['left'] + 1 - configuration['silk_fab_offset'], 'y': body_edge_out['top'] - configuration['silk_fab_offset']},
        {'x': -(C/2) - offset, 'y': body_edge_out['top'] - configuration['silk_fab_offset']}
    ]

    kicad_mod.append(PolygoneLine(polygone=poly_left_top,
        width=configuration['silk_line_width'], layer="F.SilkS"))

    poly_right_bottom = [
        {'x': (C/2) + offset, 'y': body_edge_out['bottom'] + configuration['silk_fab_offset']},
        {'x': body_edge_out['right'] + configuration['silk_fab_offset'], 'y': body_edge_out['bottom'] + configuration['silk_fab_offset']},
        {'x': body_edge_out['right'] + configuration['silk_fab_offset'], 'y': tab_size[1] / 2 + configuration['silk_fab_offset']}
    ]

    kicad_mod.append(PolygoneLine(polygone=poly_right_bottom ,
        width=configuration['silk_line_width'], layer="F.SilkS"))

    poly_right_top = [
        {'x': body_edge_out['right'] + configuration['silk_fab_offset'], 'y': -tab_size[1] / 2 - configuration['silk_fab_offset']},
        {'x': body_edge_out['right'] + configuration['silk_fab_offset'], 'y': body_edge_out['top'] - configuration['silk_fab_offset']},
        {'x': (C/2) + offset, 'y': body_edge_out['top'] - configuration['silk_fab_offset']}
    ]

    kicad_mod.append(PolygoneLine(polygone=poly_right_top ,
        width=configuration['silk_line_width'], layer="F.SilkS"))

    ######################## CrtYd Layer ###########################
    CrtYd_offset = configuration['courtyard_offset']['connector']
    CrtYd_grid = configuration['courtyard_grid']

    poly_yd = [
        {'x': roundToBase(tab['left'] - CrtYd_offset, CrtYd_grid), 'y': roundToBase(-5 - CrtYd_offset, CrtYd_grid)},
        {'x': roundToBase(tab['left'] - CrtYd_offset, CrtYd_grid), 'y': roundToBase(5 + CrtYd_offset, CrtYd_grid)},
        {'x': roundToBase(tab['right'] + CrtYd_offset, CrtYd_grid), 'y': roundToBase(5 + CrtYd_offset, CrtYd_grid)},
        {'x': roundToBase(tab['right'] + CrtYd_offset, CrtYd_grid), 'y': roundToBase(-5 - CrtYd_offset, CrtYd_grid)},
        {'x': roundToBase(tab['left'] - CrtYd_offset, CrtYd_grid), 'y': roundToBase(-5 - CrtYd_offset, CrtYd_grid)}
    ]

    kicad_mod.append(PolygoneLine(polygone=poly_yd,
        layer='F.CrtYd', width=configuration['courtyard_line_width']))

    ######################### Text Fields ###############################
    cy1 = roundToBase(body_edge_out['top'] - 1 - configuration['courtyard_offset']['connector'], configuration['courtyard_grid'])
    cy2 = roundToBase(body_edge_out['bottom'] + 1 + configuration['courtyard_offset']['connector'], configuration['courtyard_grid'])

    addTextFields(kicad_mod=kicad_mod, configuration=configuration, body_edges=body_edge_out,
        courtyard={'top':cy1, 'bottom':cy2}, fp_name=footprint_name, text_y_inside_position='center')

    ##################### Write to File and 3D ############################
    model3d_path_prefix = configuration.get('3d_model_prefix','${KISYS3DMOD}/')

    lib_name = configuration['lib_name_format_string'].format(series=series, man=manufacturer)
    model_name = '{model3d_path_prefix:s}{lib_name:s}.3dshapes/{fp_name:s}.wrl'.format(
        model3d_path_prefix=model3d_path_prefix, lib_name=lib_name, fp_name=footprint_name)
    kicad_mod.append(Model(filename=model_name))

    output_dir = '{lib_name:s}.pretty/'.format(lib_name=lib_name)
    if not os.path.isdir(output_dir): #returns false if path does not yet exist!! (Does not check path validity)
        os.makedirs(output_dir)
    filename =  '{outdir:s}{fp_name:s}.kicad_mod'.format(outdir=output_dir, fp_name=footprint_name)

    file_handler = KicadFileHandler(kicad_mod)
    file_handler.writeFile(filename)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='use confing .yaml files to create footprints.')
    parser.add_argument('--global_config', type=str, nargs='?', help='the config file defining how the footprint will look like. (KLC)', default='../../tools/global_config_files/config_KLCv3.0.yaml')
    parser.add_argument('--series_config', type=str, nargs='?', help='the config file defining series parameters.', default='../conn_config_KLCv3.yaml')
    args = parser.parse_args()

    with open(args.global_config, 'r') as config_stream:
        try:
            configuration = yaml.safe_load(config_stream)
        except yaml.YAMLError as exc:
            print(exc)

    with open(args.series_config, 'r') as config_stream:
        try:
            configuration.update(yaml.safe_load(config_stream))
        except yaml.YAMLError as exc:
            print(exc)

    configuration['lib_name_format_string'] = 'Backbone_Connector_{man:s}'

    idx = 0
    for pincount in pins_per_row_range:
        generate_one_footprint(idx, pincount, configuration)
        idx += 1
